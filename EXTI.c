﻿http://we.easyelectronics.ru/STM32/stm32-udobnye-vneshnie-preryvaniya.html
//STM32 Удобные внешние прерывания

/*Почему сброс флага прерывания Вы делаете в конце обработчика?
 Я сделал как Вы и у меня обработчик на одно событие вызывается дважды.*/

void EXTI0_IRQHandler(void) {
GPIOC->ODR ^= GPIO_ODR_ODR13; //делаем что-то полезное…
EXTI->PR |= EXTI_PR_PR0; //сброс флага события
}

/*А когда сначала сброс флага, а потом полезная работа, 
то фсьо норм. Обработчик вызывается как и положено один раз на событие.*/

void EXTI1_IRQHandler(void) {
EXTI->PR |= EXTI_PR_PR1; //сброс флага события
GPIOC->ODR ^= GPIO_ODR_ODR13; //делаем что-то полезное…


//TODO из инкубатора
/*inline void resetIrqExti95(void)
 { ////сброс прерывания дребезга контактов***********
 BIT_BAND_PER(EXTI->PR, EXTI_PR_PR7) = 1;
 BIT_BAND_PER(EXTI->PR, EXTI_PR_PR8) = 1;
 BIT_BAND_PER(EXTI->PR, EXTI_PR_PR9) = 1;
 //EXTI->PR = EXTI_PR_PR7;     EXTI->PR = EXTI_PR_PR8;     EXTI->PR = EXTI_PR_PR9;
 //FIXME использовать строку ниже для енкодера
 HAL_NVIC_ClearPendingIRQ(EXTI9_5_IRQn); //  функция сбрасывает бит в регистре прерываний, для EXTI0_I
 HAL_NVIC_EnableIRQ(EXTI9_5_IRQn); //разрешаем прерывание EXTI5_9
 }*/

//TODO из инкубатора
/*Callback EXTI - два варианта колбэка, первый кажется лучше, но видимо антидребезг обесечивается в задаче 1 задержками.
 * по сути первый вариант тот же, что и был в обработчике, только перенесен в колбэк
 *
 * Вариант 2 колбэка предусматривает защиту от дребезга, надо поробовать убрать (уменьшить) задержки в задаче 1
 * и проверить стабильность
 *
 * обратить внимание на EnabkeIRQ - ставить только выходах из обработчика без возврата в задачу, т.к. в задаче есть
 * свое разрешение прерывания после обработки результатов нажатия кнопок, иначе коллизия, все циклится
 */
/*void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
 {
 HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);

 if (GPIO_Pin == GPIO_PIN_7 || GPIO_Pin == GPIO_PIN_8
 || GPIO_Pin == GPIO_PIN_9)
 {
 key123 = GPIO_Pin >> 7 & 0b00000111; //
 if (numMenu == 0)
 {
 xSemaphoreGiveFromISR(key123Handle, pdFAIL);
 } //семафор - ???,
 }
 else
 {
 key123 = 0;
 HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
 }
 }*/

/*void HAL_GPIO_EXTI_Callback(uint16_t GPIO_Pin)
 {
 HAL_NVIC_DisableIRQ(EXTI9_5_IRQn);
 if (GPIO_Pin == GPIO_PIN_7 || GPIO_Pin == GPIO_PIN_8
 || GPIO_Pin == GPIO_PIN_9)
 {
 for (uint8_t i = 0; i < 140;)
 {
 if ((Key1_GPIO_Port->IDR & Key1_Pin)
 || (Key2_GPIO_Port->IDR & Key2_Pin)
 || (Key3_GPIO_Port->IDR & Key3_Pin))
 {
 i++;
 }
 else
 {
 key123 = 0;
 HAL_NVIC_EnableIRQ(EXTI9_5_IRQn);
 return;
 }
 }

 key123 = GPIO_Pin >> 7 & 0b00000111; //
 if (numMenu == 0)
 {
 xSemaphoreGiveFromISR(key123Handle, pdFAIL);
 } //семафор - ???,
 }
 else
 {
 key123 = 0;
 }
 }*/
